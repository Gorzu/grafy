#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <string>
#include <chrono>

#define MAX_INT 2000000000    // bardzo duza wartość liczby całkowitej (~=nieskończoność)

using namespace std;
using namespace std::chrono;



 /**************************************\
|******** Podstawowe funkcje ************|
 \**************************************/


// struktura reprezentująca pojedynczą listę sąsiedztwa
struct list_adj
{
	int v;  // sąsiedzi
	int w;  // waga
	
	list_adj* next;
};


// zapisuje grafy do pliku
void PrintData (int verticies, int edgesNumber, fstream& stream)
{
	int executions = edgesNumber - verticies + 1;

	int tmp=0;
	int vertixNumber = verticies;
	int counter=0;

	stream.open("data.txt", ios_base::app);

	// wypisanie liczby wierzchołków i krawędzi oraz losowego wierzchołka startowego
	stream << verticies << " " << edgesNumber << " " << rand()%verticies << endl;


	//pseudolosowy wierzchołek startowy oraz wagi krawędzi
	for(int i=0; i<verticies-1 && i<edgesNumber-1; i++)
		stream << i << " " << i+1 << " " << rand()%100 + 1 << endl;


	while(counter < executions)
	{
		tmp=0;

		while(counter < executions && tmp < vertixNumber-2)
		{
			stream << vertixNumber-1 << " " << tmp << " " << rand()%10 + 1 << endl;
			counter++;
			tmp++;
		}

		vertixNumber--;
	}
	stream.close();

}


/* fukcja generuje generuje zadaną liczbę losowych wierzchołków oraz krawędzi
 następnie wywołuje 100 instancji funkcji PrintData */
void GenerateData(fstream& stream)
{
	// wartości zadane w poleceniu projektu
	int verticies[5] = {10, 50, 100, 500, 1000};
	int maxEdges[5];
	int edges[5][4];
	int fillPercent[4] = {25, 50, 75, 100};  // procentowe wypełnienie zadanych przypadków

	stream.open("data.txt", ios_base::out);
	stream.close();


	for(int i=0; i<5; i++)
	{
		maxEdges[i] = (verticies[i] * (verticies[i]-1)) / 2;

		for(int j=0; j<4; j++)
		{
			edges[i][j] = (fillPercent[j] * maxEdges[i]) / 100;

			//for(int x=0; x<100; x++)
				PrintData(verticies[i], edges[i][j], stream);
		} 
	}
}



void ShowRoute(int* precursor, int* cost, int top)  // poprzednik, koszt dojścia, wierzch
{
    int* preview = new int [top];  ///alokacja pamięci dla tablicy dynamicznej do wyświetlania ścieżki

    int n=0;
    int m;

    // koszt dojścia wierzchołku startowego = 0

    for(int i=0; i<top; i++) 
    {
        cout << i <<": ";
        for(m = i; m != -1 && n < top; m = precursor[m]) 
        	preview[n++] = m;

        while(n)
        	cout << preview[--n] << " ";

        cout << "koszt dojścia: " << cost[i] << endl;
    }
    delete [] preview; 
}


 /**************************************\
|********* Lista sąsiedztwa *************|
 \**************************************/


/* lista sąsiedztwa dla algorytmu Bellmana-Forda */
void BF_List(int top, int edge, int start, list_adj** tab_list)
{
    list_adj* neighbours;

    int max_int = MAX_INT;

    int* cost;
    cost = new int [top];

    int* precursor;
    precursor = new int [top];

    for(int i=0; i<top; i++)
    {
        precursor[i] = -1;   // brak ujemnych poprzedników  
        cost[i] = max_int;   // początkowe koszty dojscia ~= nieskończoność (maksymalna wartość integer)
    }

    cost[start] = 0;           ///zerujemy koszt dojscia do naszego wierzcholka startowego

    for(int i=1; i<top; i++)
    {
        for(int j=0; j<top; j++)    ///przeszukujemy liste sasiedztwa w poszukiwaniu krawedzi dla
                                            ///danego wierzcholka relaksujac kolejne krawedzie
        {
            for(neighbours = tab_list[j]; neighbours; neighbours = neighbours->next)
            {
                if(cost[j] != max_int && cost[neighbours->v] > cost[j] + neighbours->w)
                {
                    cost[neighbours->v] = cost[j] + neighbours->w;
                    precursor[neighbours->v] = j;
                }
            }
        }
    }
}


/* Wyświetlanie tablicy list sąsiedztwa */
void ShowList(list_adj** tab_list, int top)
{ 
    list_adj* list_w;
    cout<< "lista:\n";

    for(int i=0; i<top; i++)
    {
        cout << "tab[" << i << "] = ";
        list_w = tab_list[i];

        while(list_w)
        {
          cout << list_w->v <<" ";
          list_w = list_w->next;
        }

        cout << endl;
    }

    delete [] list_w;
}


/* Tworzenie list dla grafu nieskierowanego */
void GraphList(int& top, int& edge, int& start, int** graph, list_adj** tab_list, list_adj* list)
{
    int v1, v2, weight;


    for(int i=0; i<top; i++)  
        tab_list[i] = NULL;

    
    // stworzenie listy dla grafu skierowanego
    for(int i=0; i<edge; i++) 
    {
        v1 = graph[i][0];   // wczytanie danych z reprezentacji grafu do pliku
        v2 = graph[i][1];
        weight = graph[i][2];

        list = new list_adj;

        list->v = v2;
        list->w = weight;
        list->next = tab_list[v1];
        tab_list[v1] = list;
    }


    for(int i=0; i<edge; i++)
    {
        v2 = graph[i][0]; 
        v1 = graph[i][1];
        weight = graph[i][2];

        list = new list_adj;

        list->v = v2;
        list->w = weight;
        list->next = tab_list[v1];
        tab_list[v1] = list;
    }                          
    //ShowList(tab_list, top);
}

/* Pomiar czasu działania algorytmu wyznaczania najkrótszej drogi
dla reprezentacji grafu w postaci listy sąsiedztwa*/
void DurationList(int** graph, int& top, int& edge, int& start, fstream& output)
{ 
    list_adj** tab_list;
    list_adj* list, *deleting;  // reprezentacja grafu w postaci listy sąsiedztwa 

    tab_list = new list_adj *[top];

    GraphList(top, edge, start, graph, tab_list, list); // tworzenie tablicy dynamicznej list sąsiedztwa
    high_resolution_clock::time_point p1 = high_resolution_clock::now();  // pobranie czasu z komputera przed wykonaniem algorytmu    
    BF_List(top, edge, start, tab_list);   // znalezienie najkrotszej sciezki
    high_resolution_clock::time_point p2 = high_resolution_clock::now();  // ponowne pobranie czasu z komputera


      for(int i = 0; i < top; i++) ///usuwamy listy z tablicy list
      {
        list = tab_list[i];
        while(list)
        {
          deleting = list;
          list = list->next;
          delete deleting;
        }
      }
        delete [] tab_list;   // zwolnienie pamięci

    duration<double> time_span = duration_cast<duration<double>>(p2 - p1); // obliczenie czasu trwania algorytmu

    output << "Czas trwania dla listy przy: "<< top <<" i: "<< edge*100/(top*(top-1)/2)<<"% wypelnienia wyniósł: "<< time_span.count() << " sekund.";
}


void** LoadGraph(int** graph, int& top, int& edge, int& start, ifstream& input)
{
    for(int i=0; i<edge; i++)    ///wczytanie grafu z pliku do dynamicznej dwuwymiarowej tablicy
    {
        input >> graph[i][0];
        input >> graph[i][1];
        input >> graph[i][2];

        // wyświetlenie grafu
        /*cout<<graph[i][0]<<" ";
        cout<<graph[i][1]<<" ";
        cout<<graph[i][2]<<endl;*/ 
    }
}


 /**************************************\
|******** Macierz sąsiedztwa ************|
 \**************************************/



void BF_Matrix(int** graph_m, int top, int edge, int start)
{

    int max_int = MAX_INT;

    int* cost; // koszt dojścia
    cost = new int [top];

    int* precursor;
    precursor = new int [top];

    for(int i=0; i<top; i++)
    {
        precursor[i] = -1; 
        cost[i] = max_int;   // nieskończone koszty dojścia
    }
   		 cost[start] = 0;   // koszt dojścia wierzcholka startowego = 0

    for(int i=1; i<top; i++) 
    {
        for(int j=0; j<top; j++) 
        {
            for(int v=0; v<top; v++)    // przeszukiwanie macierzy sąsiedztwa w poszukiwaniu krawędzi dla danego wierzchołka
            {
                if(cost[j] != max_int && graph_m[v][j] != 0 && cost[v] > cost[j] + graph_m[v][j])
                {
                    cost[v] = cost[j] +  graph_m[v][j];
                    precursor[v] = j;
                }
            }
        }
    }
    	
    /*
    cout <<"Macierz sasiedztwa:\n";
    ShowRoute(precursor, cost, top);
    */
}


/* Wyświetlenie macierzy sąsiedztwa */
void ShowMatrix(int** graph_m, int top)
{

    for(int i=0; i<top; i++)
    {
        for(int  j=0; j<top; j++)
        {
            cout << graph_m[i][j] <<" ";
        }
        cout << endl;
    }
}


void** GraphMatrix(int& top, int& edge, int& start, int** graph, int** graph_m)
{
    int v1, v2, weight;
    int p=0;

    for (int i=0; i<top; ++i)   // wyzerowanie krawędzi
    {
        for(int j=p; j<top; ++j)
        {
            graph_m[i][j] = 0;
            graph_m[j][i] = 0;
        }
        p++;
    }

    for (int i = 0; i < edge; ++i)
    {
        v1 = graph[i][0];        // wczytanie danych z pliku
        v2 = graph[i][1];
        weight = graph[i][2];

        graph_m[v1][v2] = weight;  // wpisanie danych do macierzy sasiedztwa dla grafu nieskierowaniego
        graph_m[v2][v1] = weight;  // dla skierowanego wylaczyc te linijke
    }
    //ShowMatrix(graph_m, top);
}


/* Pomiar czasu działania algorytmu wyznaczania najkrótszej drogi
dla reprezentacji grafu w postaci macierzy sąsiedztwa*/
void DurationMatrix(int** graph, int& top, int& edge, int& start, fstream& output)
{

    int** graph_m;
    graph_m = new int *[top];

      for(int i=0; i<top; ++i)
        	graph_m[i] = new int [top];
    	
	
	GraphMatrix(top, edge, start, graph, graph_m);  // stworzenie macierzy sąsiedztwa
    high_resolution_clock::time_point p1 = high_resolution_clock::now();        
    BF_Matrix(graph_m, top, edge, start);           // znalezienie najkrotszej sciezki
    high_resolution_clock::time_point p2 = high_resolution_clock::now();

     for(int  i=0; i<top; ++i) // zwolnienie pamięci (usunięcie macierzy sąsiedztwa)
        	delete [] graph_m[i];
    	
    delete [] graph_m;

    duration<double> time_span = duration_cast<duration<double>>(p2 - p1);
    //output << "Czas trwania dla macierzy przy: "<< top <<" i: "<< edge*100 / (top*(top-1) /2)<<"% wypełnienia wyniósł: "<< time_span.count() << " sekund" << endl;
    output << " Dla macierzy: " << time_span.count() << " sekund." <<endl;
}


 /**************************************\
|*********** Pomiar danych **************|
 \**************************************/

/* inicjacja wyjściowego strumienia danych do pliku "pomiar.txt" z prawem do dopisywania
pobranie danych ze strumienia wejściowego "data.txt"
alokacja pamięci, wywołanie funkcji mierzących czas działania algorytmu, zwolnienie pamięci */
void FindRoute(ifstream& input)
{
    fstream output;
    int top, edge, start;
    int** graph;

    output.open("pomiar.txt", ios_base::app);  // otwarcie strumienia wyjsciowego do pliku pomiar.txt
                                      
    
    for(int i = 0; i<20; i++)
    {
        input >> top;        // wczytanie parametrów do pliku
        input >> edge;
        input >> start;

        graph = new int* [edge];    // zaalokowanie pamieci dla tablicy dynamicznej dwuwymiarowej

        for(int j=0; j<edge; j++)
            graph[j] = new int [3];
        

        LoadGraph(graph, top, edge, start, input);           // inicjujemy wczytywanie grafu z pliku
        DurationList(graph, top, edge, start, output);       // inicjujemy szukanie drogi dla listy sasiedztwa
        DurationMatrix(graph, top, edge, start, output);     // inicjujemy szukanie drogi dla macierzy sasiedztwa


        for(int z=0; z<edge; ++z) // zwolnienie pamięci
            delete [] graph[z];
        
        delete [] graph;
    }
    output.close();    // zamknięcie strumienia wyjściowego
}


void Measure(ifstream& stream)
{
    stream.open("data.txt", ios_base::in); // otwarcie strumienia danych

    FindRoute(stream);  // szukanie drogi
    stream.close();  // zamknięcie strumienia danych
}


 /**************************\
|********* MAIN *************|
 \**************************/

int main()
{
	fstream data;
	ifstream input;

	srand(time(NULL));

	menu:  

	int choice = 0;
	cout << "Jaką operację chcesz wykonać?: " << endl <<endl;
	cout << "1. Wygenerowanie danych do pliku\n";
	cout << "2. Pomiar (wymaga wcześniejszego wygenerowania danych)\n";
	cout << "3. Wyjdź\n";
	cout << "Twój wybór: ";
	cin >> choice;

	 while(choice != 1 && choice != 2 && choice != 3)
	{
		cout << "Taka opcja nie istnieje. Wybierz jeszcze raz: ";
		cin >> choice;
		cout << endl;
	}

	switch(choice)
	{
		case 1:
		{
			GenerateData(data);
			cout << "Wygenerowano dane" << endl;
			break;
		}

		case 2:
		{
			Measure(input);
			cout << "Dokonano pomiaru" << endl;
			break;
		}
		case 3: break;
	}

	char mark;
	cout << "Chcesz kontynuować testy? (T/N): ";
	cin >> mark;

	if(mark == 'T' || mark == 't')
		goto menu;


	return 0;
}